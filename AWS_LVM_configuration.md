## LVM (Logical Volume Management)


All new instances in AWS are configured to use LVM. This is enabled when a new instance starts through the _**bootstrap.sh**_ (etc/templates/bootstrap.sh.erb) script provided by the deploy tool. The directories /opt and /var/log/upnxt are mounted on these logical volumes:

	/dev/mapper/ephemeral-opt on /opt type ext3 (rw)
	/dev/mapper/ephemeral-log on /var/log/upnxt type ext3 (rw)

The LVM setup looks like this:

	$ pvdisplay 
	--- Physical volume ---
	PV Name               /dev/sda2
	VG Name               ephemeral
	PV Size               29.99 GiB / not usable 4.50 MiB
	Allocatable           yes (but full)
	PE Size               4.00 MiB
	Total PE              7677
	Free PE               0
	Allocated PE          7677
	PV UUID               tnITjj-0W4F-5xnO-EDAC-R4do-IgTK-NTPsdU
	
	$ lvdisplay
	--- Logical volume ---
	LV Path                /dev/ephemeral/log
	LV Name                log
	VG Name                ephemeral
	LV UUID                IDU7rg-JMzd-nLQr-qvgU-0pdJ-tWay-4KlT2s
	LV Write Access        read/write
	LV Creation host, time ip-10-4-2-182, 2014-08-21 06:03:30 +0000
	LV Status              available
	# open                 1
	LV Size                1.50 GiB
	Current LE             384
	Segments               1
	Allocation             inherit
	Read ahead sectors     auto
	- currently set to     256
	Block device           253:0
	
	--- Logical volume ---
	LV Path                /dev/ephemeral/opt
	LV Name                opt
	VG Name                ephemeral
	LV UUID                zwIq0U-6rzx-XLCZ-Pfhm-XGhu-DkOK-c4D9fG
	LV Write Access        read/write
	LV Creation host, time ip-10-4-2-182, 2014-08-21 06:03:30 +0000
	LV Status              available
	# open                 1
	LV Size                28.49 GiB
	Current LE             7293
	Segments               1
	Allocation             inherit
	Read ahead sectors     auto
	- currently set to     256
	Block device           253:1


## Extending a volume

First create an EBS volume through the AWS console and attach it to the instance on /dev/sdg

To grow the opt volume:

	# pvcreate /dev/sdg
	# vgextend ephemeral /dev/sdg
	# lvextend -l +100%FREE /dev/ephemeral/opt
	# resize2fs /dev/ephemeral/opt
	
To grow the log volume:

	# pvcreate /dev/sdg
	# vgextend ephemeral /dev/sdg
	# lvextend -l +100%FREE /dev/ephemeral/log
	# resize2fs /dev/ephemeral/log
