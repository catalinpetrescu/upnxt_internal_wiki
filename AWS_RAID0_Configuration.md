## Configure RAID0

Create the EBS volumes and attach them to your instance. You can check which block devices are assigned with the command:
	
	# lsblk

create the raid 0 configuration:
 
	# mdadm --create --verbose /dev/md0 --chunk=64 --level=0 --raid-devices=2 /dev/xvdf /dev/xvdg
	# cat /proc/mdstat
	# mkfs.ext4 /dev/md0
	

## Move splunk data to new raid disks

	# mkdir -p /mnt/raid
	# mount /dev/md0 /mnt/raid
	# /etc/init.d/splunk stop

Wait for IO to finish on the splunk disk
	
	# iostat -x 2
	
Cleanup left-over session files to speed up the copy

	# find /opt/splunk/var/run/splunk/ -name 'session*' -print0 | xargs -0 rm
	
Copy the data

	# cp -av /opt/splunk/var/* /mnt/raid/
	
Put the new disk in place of the old one

	# umount /mnt/raid
	# umount /opt/splunk/var

Edit /etc/fstab

	/dev/md0	/opt/splunk/var ext4 defaults,nofail 0 2
	
Restart splunk

	# /etc/init.d/splunk start


## Other documentation

[Cheat sheet](http://www.ducea.com/2009/03/08/mdadm-cheat-sheet/)

[CentOS RAID configuration guide](http://www.centos.org/docs/5/html/Installation_Guide-en-US/s1-s390info-raid.html)
