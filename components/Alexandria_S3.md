# Alexandria S3

* documentation: [upnxt_storage_alexandria](https://bitbucket.org/unifiedpost/upnxt_storage_alexandria)
* bitbucket: `git clone git@bitbucket.org:unifiedpost/upnxt_storage_alexandria.git`


## Network

### Incoming

* TCP/8090 (alexandria server)
* TCP/8091 (alexandria server healthcheck)

### Outgoing

* TCP/443 (AWS S3)


## Requirements

### Software

* Centos 6.x
* Java (defaults: -Xmx384m -Xss512k -XX:+UseCompressedOops)

### Hardware

* Memory: 512MB
* Disk: 5G
    * logs: /var/log/alexandria (5G)


## Dependencies

### Required

* AWS account + S3 bucket

### Optional

* Graphite


## Configuration

* ALEX_BUCKET: amazon aws s3 bucket
* ALEX_ENDPOINT: amazon aws s3 endpoint
* ALEX_REGION: amazon aws s3 Location Constraint
* ALEX_AWS_MAX_CONNECTIONS: amazon aws max connections
* ALEX_AWS_CONNECTION_TIMEOUT: amazon aws connection timeout
* ALEX_PUBLIC_URL: alexandria public base url
* GRAPHITE_HOST: graphite host
* GRAPHITE_PORT: graphite port - default: 2003
* GRAPHITE_PREFIX: graphite prefix
* GRAPHITE_PERIOD: graphite period (in seconds) - default: 60

! Note that the access to the bucket is regulated by an IAM role in AWS so that no credentials need to be transfered to the server !

## Test Results

All tests were run on a vagrant instance with the above hardware requirements


### Throughput


### Log file growth

* 15 GET requests -> 7KB of data
* 15 PUT requests -> 100KB of data 
