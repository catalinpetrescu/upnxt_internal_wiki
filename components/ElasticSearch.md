# ElasticSearch

* home page: [ElasticSearch](http://www.elasticsearch.org)
* documentation: [guide](http://www.elasticsearch.org/guide/)
* download: [download](http://www.elasticsearch.org/download/)


## Network

### Incoming

* TCP/9200 (elasticsearch server)
* TCP/9300 (elasticsearch server)

### Outgoing

* TCP/9300 (elasticsearch server)


## Requirements

### Software

* Centos 6.x
* Java (defaults: -Xms256m -Xmx1g -Xss256k)

### Hardware

* Memory: 1GB
* Disk: 15G
    * logs: /var/log/elasticsearch (5G)
    * data: /var/lib/elasticsearch (10G)


## Dependencies

### Required


### Optional



## Configuration

config file: /etc/elasticsearch/elasticsearch.yml

```
cluster.name: my-cluster

node.name: 33.33.33.101
node.max_local_storage_nodes: 1
node.zone: zone1

network.publish_host: 33.33.33.101

index.number_of_shards: 5
index.number_of_replicas: 2

discovery:
  zen.ping.unicast.hosts: [ '33.33.33.101:9300', '33.33.33.102:9300', '33.33.33.103:9300']

path:
  logs: /var/log/elasticsearch
  data: /var/lib/elasticsearch
```

## Test Results

All tests were run on a vagrant instance with the above hardware requirements


### Throughput


### Log file growth
