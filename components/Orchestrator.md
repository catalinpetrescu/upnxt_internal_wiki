# Orchestrator

## Network

### Incoming

* TCP/22 (git push over SSH)
* TCP/8040 (orchestrator server)

### Outgoing

* TCP/5432 (postgresql server)
* TCP/6379 (redis server)
* TCP/8080 (cmis server)
* TCP/8030 (alexandria server - short term)
* TCP/8090 (alexandria server - long term)


## Requirements

### Software

* Centos 6.x
* Ruby 1.9.3

### Hardware

* Memory: 256MB
* Disk: 2G
    * logs: /var/log/orchestrator/ (2G)


## Dependencies

### Required

* CMIS
* Alexandria (long and short term)

### Optional



## Configuration

* ALEXANDRIA_USER: flowrepo
* ALEXANDRIA_SECRET: flowrepo
* WORK_ALEXANDRIA_USER: flowrepo
* WORK_ALEXANDRIA_SECRET: flowrepo
* STORAGE_RUOTE_HOST: postgresql server
* STORAGE_RUOTE_DB: ruote postgres db
* STORAGE_RUOTE_USER: ruote user 
* STORAGE_RUOTE_SECRET: ruote pass
* STORAGE_SEARCH_TYPE: postgres
* STORAGE_SEARCH_HOST: postgresql server
* STORAGE_SEARCH_DBNAME: search db
* STORAGE_SEARCH_USER: search user
* STORAGE_SEARCH_SECRET: search pass
* STORAGE_WORKITEMS_TYPE: postgres
* STORAGE_WORKITEMS_HOST: postgresql server
* STORAGE_WORKITEMS_DBNAME: workitem storag db
* STORAGE_WORKITEMS_USER: workitem storage user
* STORAGE_WORKITEMS_PASSWORD: workitem storage pass


## Test Results

All tests were run on a vagrant instance with the above hardware requirements


### Throughput


### Log file growth
