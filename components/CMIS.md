# CMIS

* documentation: [upnxt_storage_cmis](https://bitbucket.org/unifiedpost/upnxt_storage_cmis)
* bitbucket: `git clone git@bitbucket.org:unifiedpost/upnxt_storage_cmis.git`


## Network

### Incoming

* TCP/8080 (cmis server)
* TCP/8081 (cmis server healthcheck)
* TCP/9300 (elasticsearch client)

### Outgoing

* TCP/8090 (alexandria server)
* TCP/9300 (elasticsearch server)
* TCP/9200 (elasticsearch server)


## Requirements

### Software

* Centos 6.x
* Java (defaults: -Xmx384m -Xss512k -XX:+UseCompressedOops)

### Hardware

* Memory: 512MB
* Disk: 5G
    * logs: /var/log/upncmis (5G)


## Dependencies

### Required

* ElasticSearch
* Alexandria

### Optional

* Graphite


## Configuration

* ALEXANDRIA_SERVICE_URL: alexandria service url - default: http://localhost:8030
* CMIS_ALEXANDRIA_USER: alexandria user - default: cmis
* CMIS_ALEXANDRIA_PASSWORD: alexandria password - default: cmis
* CMIS_MULTI_REPO_INDEX: use 1 elasticsearch index for all repositories - default: false
* GRAPHITE_HOST: graphite host
* GRAPHITE_PORT: graphite port - default: 2003
* GRAPHITE_PREFIX: graphite prefix
* GRAPHITE_PERIOD: graphite period (in seconds) - default: 60


## Test Results

All tests were run on a vagrant instance with the above hardware requirements


### Throughput


### Log file growth

* 1000 GET requests -> 700KB of data
* 1000 POST requests -> 5MB of data 
