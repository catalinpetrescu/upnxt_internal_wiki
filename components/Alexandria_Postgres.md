# Alexandria Postgres

* documentation: [upnxt_storage_alexandria_postgres](https://bitbucket.org/unifiedpost/upnxt_storage_alexandria_postgres)
* bitbucket: `git clone git@bitbucket.org:unifiedpost/upnxt_storage_alexandria_postgres.git`


## Network

### Incoming

* TCP/8090 (alexandria server)
* TCP/8091 (alexandria server healthcheck)

### Outgoing

* TCP/5432 (postgresql server)


## Requirements

### Software

* Centos 6.x
* Java (defaults: -Xmx384m -Xss512k -XX:+UseCompressedOops)

### Hardware

* Memory: 512MB
* Disk: 5G
    * logs: /var/log/alexandria-fs (5G)


## Dependencies

### Required

* Postgresql server

### Optional

* Graphite


## Configuration

* ALEX_PUBLIC_URL: alexandria public base url
* CONFIG_FILE: location of the postgresql config (see below)
* GRAPHITE_HOST: graphite host
* GRAPHITE_PORT: graphite port - default: 2003
* GRAPHITE_PREFIX: graphite prefix
* GRAPHITE_PERIOD: graphite period (in seconds) - default: 60

Postgresql config file:

```
database:
    driverClass: org.postgresql.Driver
    user: # postgres user
    password: # postgres password
    url: # postgres jdbc url
```

## Test Results

All tests were run on a vagrant instance with the above hardware requirements


### Throughput


### Log file growth
