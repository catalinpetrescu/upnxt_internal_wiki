# Tasks Service

* documentation: [upnxt_service_tasks](https://bitbucket.org/unifiedpost/upnxt_service_tasks)
* bitbucket: `git clone git@bitbucket.org:unifiedpost/upnxt_service_tasks.git`


## Network

### Incoming

* TCP/9393 (task service)

### Outgoing

* TCP/5432 (postgresql server)


## Requirements

### Software

* Centos 6.x
* Ruby 1.9.3

### Hardware

* Memory: 256MB
* Disk: 2G
    * logs: /var/log/service-tasks (2G)


## Dependencies

### Required


### Optional



## Configuration

* STORAGE_RUOTE_HOST: postgresql server
* STORAGE_RUOTE_DB: ruote db
* STORAGE_RUOTE_USER: ruote user
* STORAGE_RUOTE_SECRET: ruote pass


## Test Results

All tests were run on a vagrant instance with the above hardware requirements


### Throughput


### Log file growth
