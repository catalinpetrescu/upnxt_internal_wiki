# CMIS Browser

* documentation: [upnxt_frontend_cmis](https://bitbucket.org/unifiedpost/upnxt_frontend_cmis)
* bitbucket: `git clone git@bitbucket.org:unifiedpost/upnxt_frontend_cmis.git`


## Network

### Incoming

* TCP/3001 (cmis browser)

### Outgoing

* TCP/8080 (cmis server)


## Requirements

### Software

* Centos 6.x
* Ruby 1.9.3

### Hardware

* Memory: 256MB
* Disk: 2G
    * logs: /var/log/frontend-cmis-browser (2G)


## Dependencies

### Required

* CMIS

### Optional



## Configuration

* DATABASE_ADAPTER: postgresql
* DATABASE_SCHEMA: db
* DATABASE_USER: user
* DATABASE_PASS: pass
* DATABASE_SERVER: postgresql server
* DATABASE_PORT: port
* CMIS_BROWSER_URL: browser url on cmis server


## Test Results

All tests were run on a vagrant instance with the above hardware requirements


### Throughput


### Log file growth
