# Alexandria Infinispan

* documentation: [upnxt_storage_alexandria_infinispan](https://bitbucket.org/unifiedpost/upnxt_storage_alexandria_infinispan)
* bitbucket: `git clone git@bitbucket.org:unifiedpost/upnxt_storage_alexandria_infinispan.git`


## Network

### Incoming

* TCP/8090 (alexandria server)
* TCP/8091 (alexandria server healthcheck)

### Outgoing


## Requirements

### Software

* Centos 6.x
* Java (defaults: -Xmx384m -Xss512k -XX:+UseCompressedOops)

### Hardware

* Memory: 512MB
* Disk: 5G
    * logs: /var/log/alexandria (5G)


## Dependencies

### Required


### Optional

* Graphite


## Configuration

* ALEX_PUBLIC_URL: alexandria public base url
* GRAPHITE_HOST: graphite host
* GRAPHITE_PORT: graphite port - default: 2003
* GRAPHITE_PREFIX: graphite prefix
* GRAPHITE_PERIOD: graphite period (in seconds) - default: 60


## Test Results

All tests were run on a vagrant instance with the above hardware requirements


### Throughput


### Log file growth
