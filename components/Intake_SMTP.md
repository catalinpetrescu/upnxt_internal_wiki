# SMTP Intake

## Network

### Incoming

* TCP/25 (SMTP server)

### Outgoing

* TCP/8090 (alexandria server - long term)
* TCP/8040 (orchestrator server)


## Requirements

### Software

* Centos 6.x
* Java

### Hardware

* Memory: 256MB
* Disk: 5G
    * logs: /opt/upnxt/apps/james/current/apps/james/logs (5G)


## Dependencies

### Required

* Orchestrator
* Alexandria (long term)

### Optional



## Configuration

* INTAKE_ALEXANDRIA_USER: user
* INTAKE_ALEXANDRIA_OWNER: owner
* INTAKE_ALEXANDRIA_SECRET: pass
* ORCHESTRATOR_SERVICE_URL: url to orchestrator
* GRAPHITE_HOST: graphite host
* GRAPHITE_PORT: graphite port - default: 2003
* GRAPHITE_PREFIX: graphite prefix
* GRAPHITE_PERIOD: graphite period (in seconds) - default: 60


## Test Results

All tests were run on a vagrant instance with the above hardware requirements


### Throughput


### Log file growth
