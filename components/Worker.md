# Worker

## Network

### Incoming

* TCP/22 (trigger to load slug)

### Outgoing

* TCP/5432 (postgresql server)
* TCP/6379 (redis server)
* TCP/8040 (orchestrator server)
* TCP/8080 (cmis server)
* TCP/8030 (alexandria server - short term)
* TCP/8090 (alexandria server - long term)


## Requirements

### Software

* Centos 6.x
* Ruby 1.9.3

### Hardware

* Memory: 512MB
* Disk: 15G
    * logs: /var/log/???/ (5G)

depending on how many worker processes you want to start on the machine, more memory is require. (64MB per worker process)


## Dependencies

### Required

* Orchestrator
* CMIS
* Alexandria (long and short term)

### Optional



## Configuration

receives configuration from slug that is retrieved from orchestrator


## Test Results

All tests were run on a vagrant instance with the above hardware requirements


### Throughput


### Log file growth
