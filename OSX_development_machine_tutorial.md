# OSX Development Machine Tutorial

## General

* update your system to the latest OSX through software update
* configure Mail
* install Xcode through the App Store
(this is optional, installing homebrew triggers you to install the commandline tools)

		>> launch xcode to accept terms and conditions !!!!

* create machine key

		ssh-keygen
		# copy public key to buffer (to configure in e.g. bitbucket)
		cat ~/.ssh/id_rsa.pub | pbcopy


* Oh My ZSH (For ZSH lovers) for better tab completion 

		curl -L https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh | sh

* install brew http://brew.sh

		ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)”

* install cask https://github.com/caskroom/homebrew-cask

		brew install caskroom/cask/brew-cask		
		# add more alternative versions as cask source
		# needed for sunlime-text3 at this time
		brew tap caskroom/versions

* install software

		brew install git
		brew install wget
		brew install heroku-toolbelt
		brew cask install google-chrome
		brew cask install hipchat
		brew cask install sublime-text3
		brew cask install pgadmin3 
		brew cask install postgres 
		brew cask install virtualbox
		brew cask install tunnelblick
		brew cask install sourcetree	
		brew cask install Skype

* add sublime to the path

		ln -s /opt/homebrew-cask/Caskroom/sublime-text3/Build\ 3065/Sublime\ Text.app/Contents/SharedSupport/bin/subl /usr/local/bin/sublime

* git

		git config --global user.name "Your Name"
    	git config --global user.email you@example.com

* install rbenv

		brew install rbenv ruby-build
		# for bash
		echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bash_profile
		echo 'eval "$(rbenv init -)"' >> ~/.bash_profile
		# for zsh use . zshrc
		echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.zshrc
		echo 'eval "$(rbenv init -)"' >> ~/.zshrc

* install ruby

		# list versions
		rbenv install -l 
		# install ruby platform version	
		rbenv install 2.1.3
		rbenv global 2.1.3

* gems tools

		gem install bundler
		gem install foreman
		rbenv rehash
		
		# if you don't want to docs to auto install (you have another rubydoc tool)
		# You just add following line to your local ~/.gemrc file (it is in your home folder)
			gem: --no-document

* java

		brew cask install java
		brew install maven
		brew cask install intellij-idea


* configure tunnelblock => see bert documentation

dubbelclick on your config file backup

* more to opt

		brew install 
			apgdiff 
			elasticsearch 	
			gd 
			gnuplot 
			jmeter 
			leiningen 
			libpng 
			libyaml 
			openssl 
			pkg-config 
			unrar 
			watch 
			rust
		brew cask install 
			dropbox 
			f-lux  
		brew cask install firefox
		brew cask install mou
		brew cask install monolingual
		brew cask install coconutbattery
