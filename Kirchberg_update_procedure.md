# Kirchberg update procedure

## update elasticsearch: (data node)

upncmis connects to elasticsearch. When setting up new cluster, upncmis backend needs to be updated also so that /etc/relasticsearch.yml reflects the new cluster

   * monitor elasticsearch cluster
      * `deploy archive.yml -f data,3 --ssh-tunnel 9200`
      * http://localhost:9200/_plugin/head/
   * stop elasticsearch on node      
      * `/etc/init.d/elasticsearch stop`
   * verify yellow cluster state (1 unassigned)
   * verify documents are still accessible (frontend 1 and 2)
   * `deploy archive.yml -f data:elasticsearch,1 --conf`
   * verify node rejoined the cluster
   * verify green cluster state (might take a minute)
   * verify documents are still accessible (frontend 1 and 2)



## update cmis: (archive node)

url 1: https://frontend-cmis-browser-1.lu.apps.up-nxt.com

url 2: https://frontend-cmis-browser-2.lu.apps.up-nxt.com


### 1. backend (upnxt_service_cmis)

   * verify document accessible from CMIS browser (check for url 1 and url 2)
      * connect to frontend cmis url
      * browse 'krichberg/archive' repository
      * open document 10754737HK/1 (check preview)

   * take stack 1 out of loadbalancer (no actual loadbalancer at this time, faking)
   * update backend 1
      * login to host over SSH
      * `stop upncmis`
      * `rm -rf /opt/upnxt/apps/upncmis`
      * verify document remains accessible through frontend 2 during update
      * `deploy archive.yml -f archive:upncmis,1 --conf`
      * after update, check document accessible through frontend 1

   * update backend 2
      * login to host over SSH
      * `stop upncmis`
      * `rm -rf /opt/upnxt/apps/upncmis`
      * verify document remains accessible through frontend 1 during update
      * `deploy archive.yml -f archive:upncmis,2 --conf`
      * after update, check document accessible through frontend 2


### 2. frontend (upnxt_frontend_cmis)

   * verify documents accessible from CMIS browser (check for url 1 and url 2)
      * connect to frontend cmis url
      * browse 'krichberg/archive' repository
      * open document 10754737HK/1 (check preview)

   * take stack 1 out of loadbalancer (no actual loadbalancer at this time, faking)
   * update frontend 1
      * login to host over SSH
      * `stop frontend-cmis-browser`
      * `rm -rf /opt/upnxt/apps/frontend-cmis-browser`
      * verify frontend url 1 gets 502 bad request
      * verify document remains accessible through frontend 2 during update
      * `deploy archive.yml -f archive:frontend-cmis-browser,1 --conf`
      * after update, check document accessible through frontend 1

   * update frontend 2
      * login to host over SSH
      * `stop frontend-cmis-browser`
      * `rm -rf /opt/upnxt/apps/frontend-cmis-browser`
      * verify frontend url 2 gets 502 bad request
      * verify document remains accessible through frontend 1 during update
      * `deploy archive.yml -f archive:frontend-cmis-browser,2 --conf`
      * after update, check document accessible through frontend 2


## possible problems for frontend: 

   * shared db -> db refactorings that break backward compatibility will cause application failure !


