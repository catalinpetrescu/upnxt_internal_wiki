## add secondary IP to ENI of machine

in AWS console, under EC2 -> Network Interfaces, find an IP address that's free. Select the ENI you want to change and go to:

	- Action
	- Manage Private IP Addresses
	- Assign new IP
	- enter the value for the static ip
	- Yes, update

## Add the alias on the host

On the host, manually add the secondary ip as an alias:

	# ifconfig eth0:1 10.4.200.10 netmask 255.255.255.240


## Make rebootable

Create the /etc/sysconfig/network-scripts/ifcfg-eth0:1 file from the existing ifcfg-eth0 file:

	DEVICE="eth0:1"
	BOOTPROTO="none"
	IPADDR="10.4.200.11"
	NETMASK="255.255.255.240"
	MTU="1500"
	ONBOOT="yes"
	TYPE="Ethernet"

## Test

Restart alias interface

	# ifdown eth0:1
	# ifup eth0:1