# IAM

## certificates

Generate X.509 certificate from private key

<http://docs.aws.amazon.com/IAM/latest/UserGuide/Using_UploadCertificate.html>

	$ openssl pkcs8 -topk8 -nocrypt -inform PEM -in deploy-eu-west.pem -out deploy-eu-west-pkcs8.pem
	$ openssl req -new -x509 -nodes -sha1 -days 365 -key deploy-eu-west.pem -outform PEM > deploy-eu-west.cert

Press [Enter] for all questions

# AMI

## Create from instance store AMI

Launch a pre-existing instance store AMI (eg: ami-029f9476 from http://www.bashton.com/blog/2013/centos-6-4-ami-available/), login to the instance and become root

	# yum -y update
	# yum -y install ruby
	# rpm -ivh http://yum.puppetlabs.com/el/6/products/i386/puppetlabs-release-6-7.noarch.rpm
	# yum -y install puppet
	# yum -y reinstall ca-certificates
	# easy_install awscli
	# rpm -Uvh http://s3.amazonaws.com/ec2-downloads/ec2-ami-tools.noarch.rpm
	# yum -y reinstall aws-amitools-ec2.noarch
	# export PATH=$PATH:/usr/local/bin
	# yum clean all

Remove Postfix

	# chkconfig postfix off
	# service postfix stop


Edit /etc/fstab to mount instance storage on /opt

	# vi /etc/fstab
	# mount /opt
	# umount /media/ephemeral0

Edit /etc/sudoers. Add !requiretty for root

	# vi /etc/sudoers
	# Defaults:root !requiretty
	# Defaults:ec2-user !requiretty

Make resource limits big enough
	
	# rm -f /etc/security/limits.d/90-nproc.conf
	# cat > /etc/security/limits.d/99_upnxt.conf <<-EOT
	* hard nofile 65536
	* soft nofile 65536
	* hard nproc 65536
	* soft nproc 65536
	EOT


	# export ACCOUNT=496376733012 IMAGE=ami-0001
	# mkdir /tmp/cert
	# chmod 777 /tmp/cert
	# scp -i deploy-eu-west ~/.ssh/deploy-eu-west.pem ~/.ssh/deploy-eu-west.cert ec2-user@ec2-54-247-135-169.eu-west-1.compute.amazonaws.com:/tmp/cert
	# mkdir -p /opt/image
	# ec2-bundle-vol -k /tmp/cert/deploy-eu-west.pem -c /tmp/cert/deploy-eu-west.cert -u $ACCOUNT -r x86_64 -e /tmp/cert,/opt/image -d /opt/image
	# ec2-upload-bundle -b upnxt-ami/$IMAGE -m /opt/image/image.manifest.xml -a $AWS_ACCESS_KEY_ID -s $AWS_SECRET_ACCESS_KEY --location EU 

	
	# cd /opt
    # wget http://s3.amazonaws.com/ec2-downloads/ec2-api-tools.zip
    # unzip ec2-api-tools.zip 
	# export EC2_HOME=/opt/ec2-api-tools-1.6.10.1
	# export PATH=$PATH:/opt/ec2-api-tools-1.6.10.1/bin
	# rpm -Uvh jdk-7u40-linux-amd64.rpm
	# export JAVA_HOME=/usr/java/jdk1.7.0_40
	# ec2-register upnxt-ami/$IMAGE/image.manifest.xml -n 'CentOS 6.4 IS' -d 'ami-029f9476 (http://www.bashton.com/blog/2013/centos-6-4-ami-available/)' -O $AWS_ACCESS_KEY_ID -W $AWS_SECRET_ACCESS_KEY --region eu-west-1

## Create from EBS AMI

Launch a pre-existing EBS AMI (eg: ami-8aa3a8fe from http://www.bashton.com/blog/2013/centos-6-4-ami-available/), login to the instance and become root

	# yum -y update
	# yum -y install ruby
	# rpm -ivh http://yum.puppetlabs.com/el/6/products/i386/puppetlabs-release-6-7.noarch.rpm
	# yum -y install puppet
	# yum -y reinstall ca-certificates
	# yum clean all

Remove Postfix

	# chkconfig postfix off
	# service postfix stop

Edit /etc/sudoers. Add !requiretty for root

	# vi /etc/sudoers
	# Defaults:root !requiretty
	# Defaults:ec2-user !requiretty

Make resource limits big enough
	
	# rm -f /etc/security/limits.d/90-nproc.conf
	# cat > /etc/security/limits.d/99_upnxt.conf <<-EOT
	* hard nofile 65536
	* soft nofile 65536
	* hard nproc 65536
	* soft nproc 65536
	EOT

Right click the running instance and select 'Create EBS image...'


# LoadBalancer (ELB)

## Listener

HTTPS - HTTPS
Upload certificate (PEM) + private key

## Health Check

Machine level health check. No hostname on connection, so no vhost triggered on Nginx. Default vhost is used.

	listen 443 default_server;
	server_name my.vhost.com;


# Nginx

## Authentication

### Basic

Per vhost generate a `my.vhost.com.users` file.
Users file populated with output of: `htpasswd -bn ${user} ${password}`

	location / {
        auth_basic "private basic auth access";
        auth_basic_user_file /etc/nginx/my.vhost.com.users;
        ...
	}

### LDAP

Configure LDAP on Nginx machine by generating the `/etc/openldap/ldap.conf` file:

	authconfig --enableldap --enableldapauth --enablemkhomedir --ldapserver=${ldap_server} --ldapbasedn='${ldap_base_dn}' --update

Configure PAM module for Nginx and LDAP through `/etc/pam.d/nginx-ldap`:

	auth        required      pam_ldap.so
	account     required      pam_ldap.so

Configure Nginx vhost to use LDAP through PAM:

	location / {
        auth_pam "private pam access";
        auth_pam_service_name "nginx-ldap";
        ...
	}

## SSL

### Create own certificates (self-signed)

SSL Configuration (on CA server and machine where CSR is generated)

	vi /etc/pki/tls/openssl.cnf
	string_mask = pkix

Create own CA 

	cd /etc/pki/CA
	touch index.txt
	echo "01" > serial
	echo "01" > crlnumber
	openssl req -new -x509 -extensions v3_ca -keyout private/ca-cert.key -out certs/ca-cert.crt -days 365
	chmod 400 private/ca-cert.key

Create CSR

	cd /tmp
	openssl genrsa -des3 -out cert.key 1024
	openssl req -new -key cert.key -out cert.csr

Sign CSR using CA key

	cd /etc/pki/CA
	cp /tmp/cert.csr /etc/pki/CA/crl
	openssl ca -in crl/cert.csr -out newcerts/cert.pem -keyfile private/ca-cert.key -cert certs/ca-cert.crt

The `cert.key` (private key) and `cert.pem` (certificate) files can then be used in Nginx vhosts

    listen                      443;
    ssl                         on;
    ssl_protocols               SSLv2 SSLv3 TLSv1;
    ssl_ciphers                 HIGH:!aNULL:!MD5;
    ssl_prefer_server_ciphers   on;
    ssl_certificate             /etc/nginx/cert.pem;
    ssl_certificate_key         /etc/nginx/cert.key;
    ssl_session_timeout         5m;

Remove passphrase from private key

	cp cert.key cert.key.org
	openssl rsa -in cert.key.org -out cert.key
	chmod 0600 cert.key
	
Verify the certificate matches the private key

	openssl rsa -noout -modulus -in cert.key
	openssl x509 -noout -modulus -in cert.pem
	
### Upload certificates

You'll need to install the aws-sdk	gem:

	$ gem install aws-sdk
	

To get a listing of all current server certificates
	
	$ aws iam list-server-certificates
	
When you have an encrypted private key, you can decrypt it by using

	$ openssl rsa -in my.key -out my_unencrypted.key
	
You need to create a file containing both the certificate + chain

	$ cat cert.crt chain.crt > cert-with-chain.crt
	
Print the fingerprint of the certificate to use in the certificate name on AWS
	
	$ openssl x509 -noout -fingerprint -in cert-with-chain.crt | cut -d= -f2 | tr : .
	
Next you can upload the server certificate to AWS

	$ aws iam upload-server-certificate --server-certificate-name cert-{{fingerprint}}-v1 --certificate-body file://cert-with-chain.crt --private-key file://my_unencrypted.key

### Update certificate

You can't update a certificate body directly on AWS, so you'll need to upload a certificate with a new name and then switch to the new certificate


	
## VHosts

Configuration file per Vhost in `/etc/nginx/conf.d`
Load all Vhost config files from `/etc/nginx/nginx.conf`

	http {
		...
		include /etc/nginx/conf.d/*.conf;
	}

NOTE: You can indicate which vhost is the default one by adding `default_server` to the `listen` directive

	listen 443 default_server;


## Nginx gotchas

### Body size

Configurable from `/etc/nginx/nginx.conf`

	client_max_body_size 0;

### Debug logging

Configurable from `/etc/nginx/nginx.conf`

	error_log  /var/log/nginx/error.log debug;

Change binary in `/etc/init.d/nginx`

	nginx=${NGINX-/usr/sbin/nginx.debug}

### HTTP status code 400

A request is made to port 443 with protocol http instead of https. To reproduce, connect to `http://my.vhost.com:443`	