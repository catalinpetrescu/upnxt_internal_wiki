## OpenVPN Configuration

To lock down VPC environments on UAT and PROD we use a OpenVPN server. It is currently not installed by the deploy tool, so this is the procedure used to add a new server

Notes:

* Client certificates are shared on all servers
* CA certificate is shared on all servers
* EIP are used to create a non-changing endpoint for clients to connect to
* DEV uses 10.7.0.0/24, UAT uses 10.8.0.0/24 and PROD uses 10.9.0.0/24 in the server.conf

The installation instructions are taken from [here](https://www.digitalocean.com/community/tutorials/how-to-setup-and-configure-an-openvpn-server-on-centos-6)



### 1. Launching a server

We will use _**t2.micro**_ machines to limit the cost. Since these VPN servers are only used for administrative purpose, they are not in any critical path. The idea is that the VPN connection will also be used by developers once we have more experience with the server

A typical configuration will look like:

	vpn:
        count: 1
        subnet: [10.4.0.0/24]
        flavor: t2.micro
        ami: ami-d463baa3
        role: up-uat-1.2
        infrastructure:
            - name: epel
            - name: ntpd
            - name: pgdg


To start this machine, do:

	$ deploy uat.yml -f vpn,1 --up


### Create PKI store (only once)

* Create EBS volume
* Attach to instance
* Mount on /opt/upnxt/pki

setup easy-rsa:

	# yum -y install easy-rsa
	# mkdir -p /opt/upnxt/pki/easy-rsa/keys
	# cp /usr/share/easy-rsa/2.0/* /opt/upnxt/pki/easy-rsa/keys/
	# vim /opt/upnxt/pki/easy-rsa/keys/vars
	
then find the following lines and change them accordingly:

    export KEY_COUNTRY="BE"
    export KEY_PROVINCE="Brabant"
    export KEY_CITY="Leuven"
    export KEY_ORG="UP-nxt"
    export KEY_EMAIL="support@up-nxt.com"
    export KEY_OU="RnD"
    export KEY_NAME="OpenVPN"
    export KEY_CN="UP-nxt RnD"

Make sure that the system finds the openssl cnf file:

	# cp /usr/share/easy-rsa/2.0/openssl-1.0.0.cnf /opt/upnxt/pki/easy-rsa/
	

#### Clean

Prepare for generating the certificates the first time:

	# cd /opt/upnxt/pki/easy-rsa/keys
	# . vars
	# ./clean-all
	
#### Generate the CA certificate

	# ./build-ca 
	
Accept all defaults


#### Generate server certificate

	# cd /opt/upnxt/pki/easy-rsa/keys
	# . vars
	# ./build-key-server server
	
Accept all defaults

	# ./build-dh
	
### Working with client SSL certificates

#### Generate new on OpenVPN server

To be able to connect to the VPN server, you will need to generate client certificates. We use certificates that are protected by a passphrase, so you will need the person who the certificate is for to manually enter it during the generation process

	# cd /opt/upnxt/pki/easy-rsa/keys
	# . vars
	# ./build-key-pass gerrit.germis@up-nxt.com
	
Choose a passphrase
Accept other defaults

#### Generate CSR on client machine

Because we do not want to communicate the private keys (.key files), it's better to have them generated on the users machine so they will never need to be transmitted to the user. The only information that needs to be communicated is the CSR and CRT files


	# export EMAIL='my.email@unifiedpost.com'
	# openssl req -out ~/"${EMAIL}.csr" -new -newkey rsa:2048 -subj "/C=BE/ST=Brabant/L=Leuven/O=UP-nxt/OU=RnD/CN=$EMAIL" -keyout ~/"${EMAIL}.key"
	# chmod 0600 ~/"${EMAIL}.key"

Now have the user send the CSR file to they admins

#### Sign the CSR on the OpenVPN server

Put the CSR file in the easy-rsa keys directory and sign it

	# cd /opt/upnxt/pki/easy-rsa/keys
	# . vars
	# cp /tmp/my.email@unifiedpost.com.csr keys/
	# ./sign-req my.email@unifiedpost.com
	
Note that the error you get during sign-req is normal (it tries to chmod 0600 the .key file which isn't there). A .crt file will be created in the keys/ directory. Send this .crt file to the user (allowed to send over an insecure channel)


#### Revoke certificate

To add a certificate to the revocation list (CRL):
	
	# cd /opt/upnxt/pki/easy-rsa/keys
	# . vars
	# ./revoke-full someone@up-nxt.com
	...
	error 23 at 0 depth lookup:certificate revoked	
Check revocation status (R = revoked, V = valid):
	
	# cat keys/index.txt
	V	250130070253Z		01	unknown	/C=BE/ST=Brabant/L=Leuven/O=UP-nxt/OU=RnD/CN=server/name=OpenVPN/emailAddress=support@up-nxt.com
	V	250130070743Z		02	unknown	/C=BE/ST=Brabant/L=Leuven/O=UP-nxt/OU=RnD/CN=gerrit.germis@up-nxt.com/name=OpenVPN/emailAddress=support@up-nxt.com
	R	250130071708Z	150202073732Z	03	unknown	/C=BE/ST=Brabant/L=Leuven/O=UP-nxt/OU=RnD/CN=someone@up-nxt.com/name=OpenVPN/emailAddress=support@up-nxt.com
	R	250130071735Z	150202074640Z	04	unknown	/C=BE/ST=Brabant/L=Leuven/O=UP-nxt/OU=RnD/CN=someone.else@up-nxt.com/name=OpenVPN/emailAddress=support@up-nxt.com
	R	250130074538Z	150202074629Z	05	unknown	/C=BE/ST=Brabant/L=Leuven/O=UP-nxt/OU=RnD/CN=someone@up-nxt.com/name=OpenVPN/emailAddress=support@up-nxt.com


#### Distribute crl.pem

The CRL file needs to be placed on all openvpn servers so they can check if the certificate was revoked or not

On the server containing the PKI store:

	# rm -f /tmp/crl.pem
	# cp /opt/upnxt/pki/easy-rsa/keys/keys/crl.pem /tmp
	# chmod a+r /tmp/crl.pem
	
On your local machine:

	# rm -f /tmp/crl.pem
	# deploy nxt-uat-unifiedpost.com -f vpn,1 --get /tmp/crl.pem:/tmp
	# ansible-playbook -i inventories/vpn-1-2.inventory playbooks/update/update-crl-pem.yml -e crl=/tmp/crl.pem

The ansible script will copy the crl.pem to all vpn servers to the /etc/openvpn directory and upload it to S3


#### Regenerate the crl.pem

By default the CRL is only valid for 1 month. When it expires

	$ openssl crl -in crl.pem -noout -nextupdate
	nextUpdate=Mar  2 07:33:06 2016 GMT

client SSL certificates in nginx are not accepted anymore (when ssl_crl is used to validate valid certificates). To regenerate the crl.pem:

	# cd /opt/upnxt/pki/easy-rsa/keys
	# . vars
	# openssl ca -gencrl -out /tmp/crl.pem -config "$KEY_CONFIG"

This should probably be scheduled somewhere and done at least once a month. OpenVPN does not seem to have an issue with this


### 2. Install OpenVPN

The easy-rsa package is used to generate the certificates

	# yum -y install openvpn

Copy the default configuration file into the configuration directory

	# cp /usr/share/doc/openvpn-*/sample/sample-config-files/server.conf /etc/openvpn/
	# vim /etc/openvpn/server.conf

add lines (subnets depend on the VPC you need to be able to access):

	push "route 10.4.0.2 255.255.255.255"

configure DNS

	push "dhcp-option DNS 10.4.0.2"
	push "dhcp-option DOMAIN nxt.uat.unifiedpost.com"

uncomment the following lines:

    user nobody
    group nobody

change to diffie-helman 2048:

    dh dh2048.pem

add CRL check

	crl-verify crl.pem


#### Configure CCD

There is a way to push routes based on the certificate that is used to connect to the VPN server, but for some reason, routing to other nodes doesn't work correctly when using this method:

	# mkdir -p /etc/openvpn/ccd
	# vim /etc/openvpn/server.conf
	
Remove all the push routes from the main configuration file:

	; push "route 10.4.0.0 255.255.255.0"
	; push "route 10.4.1.0 255.255.255.0"                                                                                                                                                          
	; push "route 10.4.2.0 255.255.255.0"

Add route entries for all possible routes that will need to be pushed:

	client-config-dir ccd

Create a DEFAULT file

	# vim /etc/openvpn/ccd/DEFAULT
	
add the default push routes as needed:

	push "route 10.4.0.0 255.255.255.0"
	
for every certificate you can create a new file and extend the pushed routes:

	# vim /etc/openvpn/ccd/gerrit.germis@up-nxt.com
	push "route 10.4.0.0 255.255.255.0"
	push "route 10.4.1.0 255.255.255.0"                                                                                                                                                           
	push "route 10.4.2.0 255.255.255.0"	



### Copy the certificates to the config directory

	# cd /opt/upnxt/pki/easy-rsa/keys/keys
	# cp dh2048.pem ca.crt server.crt server.key crl.pem /etc/openvpn/
	
	

### Configure the VM for routing

Enable IP forwarding (routing between network interfaces)

	# vim /etc/sysctl.conf
	net.ipv4.ip_forward = 1
	# sysctl -p /etc/sysctl.conf

Enable Masquerading (faking VPN server IP address for all clients)

	# iptables -t nat -A POSTROUTING -s 10.8.0.0/24 -o eth0 -j MASQUERADE
	# /etc/init.d/iptables save
	

### 8. Start the server

	# service openvpn start
	# chkconfig openvpn on	


### 9. Associate an EIP with the new machine

From the AWS console (EC2), generate a new EIP and associate it with the newly create VPN instance. This public IP address will be used by all clients to connect to the VPN server

Also make sure that UDP port 1194 is accessible by everybody (0.0.0.0/0) in the security group for the VPN server


### 10. Client configuration

The generated client certificate files (eg: gerrit.germis@up-nxt.com.crt + gerrit.germis@up-nxt.com.key) from step 6 should be sent to the person that wants to connect to the VPN. For this reason, we keep all existing certificates in S3 (eg: _**s3://upnxt-keys/vpn/ca.crt, s3://upnxt-keys/vpn/gerrit.germsi@up-nxt.com.crt, s3://upnxt-keys/vpn/gerrit.germis@up-nxt.com.key**_) The public CA certificate should also be communicated to that person (ca.crt)

	remote 54.77.170.94 1194 udp
	persist-key
	tls-client
	pull
	ca ca.crt
	cert gerrit.germis@up-nxt.com.crt
	key gerrit.germis@up-nxt.com.key
	dev tun
	persist-tun
	comp-lzo no
	nobind

The remote directive specifies the EIP of the VPN server that the client will need to connect to.

The client should now be able to connect to the VPN server using the certificates we communicated with them