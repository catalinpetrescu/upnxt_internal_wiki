# Invoice Approval Frontend Scenario

Scenario for generating a frontend app for workflow in `git@bitbucket.org:upnxt/blueprint_invoice_approval_processing.git`.

## Setup

	git clone git@bitbucket.org:upnxt/upnxt_frontend_rich_archetype.git frontend
	cd frontend
	git remote rm origin
	git remote add origin 'git@apps.workshop.apps.up-nxt.com:app01.git'

Fetch libraries:

    bundle install
    
Edit `config/initializers/app.rb`:

	APP_NAME = "Invoice Approval"
	APP_DOMAIN = "workshop"
	APP_TYPE = "customer"
	APP_VERSION = "0.1"

Create and migrate the database:

	rake db:create
	rake db:migrate

Create a `Procfile` for foreman:

    web: rails server
    
Create a `.env` file for foreman (remote Task and CMIS services):

	TASK_SERVICE_URL=<remote task service url>
	CMIS_ATOM_URL=<remote cmis url>
	
Test the app by running `TASK_SERVICE_URL=<remote task service url> CMIS_ATOM_URL=<remote cmis url> rails server` or use `foreman start`.
	
**COMMIT POINT**

## User Management
	
Add permissions to users.

Create a migration: `rails generate migration AddPermissionsToUsers`.

	def change
	  add_column :users, :act_as_finance,    :boolean, default: false
	  add_column :users, :act_as_operations, :boolean, default: false
	  add_column :users, :act_as_ict,        :boolean, default: false
	  add_column :users, :act_as_board,      :boolean, default: false
	end
  	
Migrate the database: `rake db:migrate`.

Edit `app/models/user.rb`.

	attr_accessible :email, :password, :password_confirmation, :remember_me,
      :act_as_finance, :act_as_operations, :act_as_ict, :act_as_board

Seed the database with users in `db/seeds.rb`.

	User.create(email: 'cfo@unifiedpost.com',
	            password: 'password',
	            password_confirmation: 'password',
	            act_as_finance: true)
	
	User.create(email: 'coo@unifiedpost.com',
	            password: 'password',
	            password_confirmation: 'password',
	            act_as_operations: true)
	
	User.create(email: 'cio@unifiedpost.com',
	            password: 'password',
	            password_confirmation: 'password',
	            act_as_ict: true)
	
	User.create(email: 'ceo@unifiedpost.com',
	            password: 'password',
	            password_confirmation: 'password',
	            act_as_board: true)

Run `rake db:seed`.  
Test the app by running `foreman start`.

**COMMIT POINT**

## Tasks

	rails generate upnxt_frontend_rich_modules:tasks_install
	rails generate upnxt_frontend_rich_modules:tasks_instance my_tasks workshop invoice


	rails generate upnxt_frontend_rich_modules:task_type approval_by_finance
	rails generate upnxt_frontend_rich_modules:task_type approval_by_operations
	rails generate upnxt_frontend_rich_modules:task_type approval_by_ict  
	rails generate upnxt_frontend_rich_modules:task_type approval_by_board

Edit `app/views/common/_menu.html.erb`.

Test the app by running `foreman start`.

Edit `app/controllers/my_tasks_controller.rb`:

	def list_criteria(query)
	  { type: current_user.task_types }
	end

Edit `app/models/user.rb`:

	def task_types
	  res = []
	  res << 'approval_by_finance'    if act_as_finance
	  res << 'approval_by_operations' if act_as_operations
	  res << 'approval_by_ict'        if act_as_ict
	  res << 'approval_by_board'      if act_as_board
	  res
	end
	
Test the app by running `foreman start`.

**COMMIT POINT**

## Documents

	rails generate upnxt_frontend_rich_modules:documents_install
	rails generate upnxt_frontend_rich_modules:documents_instance archive
	rails generate upnxt_frontend_rich_modules:document_properties archive amount from due document_number

Edit `app/controllers/archive_controller.rb`:

	def domain
	  'workshop'
	end
	
	def type
	  'invoice'
	end
	
	def list_id
	  'froot'
	end

Edit `app/views/common/_menu.html.erb`.

Test the app by running `foreman start`.

**COMMIT POINT**

## Custom behaviour & custom task views

Rename one task type to `approval` and delete the others.  
Edit `config/tasks.yml`.  
Edit `app/views/task_types/approval/_snippet_for_collection.html.erb`.

	<h3>To Approve</h3>
	<!-- ko with:properties -->
	  <ul class="unstyled">
	    <li>Invoice <span data-bind="text: document_number"></span> for <strong>&euro;<span data-bind="text: amount"></span></strong></li>
	    <li>From <a data-bind="text: from"></a></li>
	    <li>Due <span data-bind="text: due"></span></li>
	  </ul>
	<!-- /ko -->

Create `app/assets/javascripts/task_types/approval.js.coffee`:

	tasks = ritchie.modules._tasks
	context = ritchie.context
	ajax = ritchie.ajax
	tasks.approval =
	  Entity: class extends tasks.Entity
	    constructor: (itemAsJS)->
	      super itemAsJS
	      # next fields are purely for the generic task views.
	      @unchangeable = ko.toJS itemAsJS
	      delete @unchangeable.properties
	    approve: ->
	      ajax.post ("#{context.apiRoot}/#{@id}/proceed"), { approved: true },
	      (->
	        if 0 < history.length then History.back() else History.replaceState null, null, "#{context.pageRoot}/@id"
	      ),
	      (-> alert "Could not proceed.")
	      return false
	    dispute: ->
	      ajax.post ("#{context.apiRoot}/#{@id}/proceed"), { disputed: true },
	      (->
	        if 0 < history.length then History.back() else History.replaceState null, null, "#{context.pageRoot}/@id"
	      ),
	      (-> alert "Could not proceed.")
	      return false
	    noop: ->
	      ajax.delete ("#{context.apiRoot}/#{@id}"), {},
	      (->
	        if 0 < history.length then History.back() else History.replaceState null, null, "#{context.pageRoot}/@id"
	      ),
	      (-> alert "Could not cancel.")
	      return false

Edit `app/assets/javascripts/task_types/approval_by_*.js.coffee` to extend `approval`.

Add `//= require task_types/approval` in `app/assets/javascripts/application.js`.

Edit `app/views/task_types/approval/_snippet_for_entity.html.erb` and replace the summary tab content with :

	  <h3 class="task-type" data-bind="text: 'To Approve'"></h3>
	
	  <!-- ko with:properties -->
	    <ul class="unstyled">
	      <li>Invoice <span data-bind="text: document_number"></span> for <strong>&euro;<span data-bind="text: amount"></span></strong></li>
	      <li>From <a data-bind="text: from"></a></li>
	      <li>Due <span data-bind="text: due"></span></li>
	    </ul>
	  <!-- /ko -->
	
	  <!-- ko with:files -->
	    <div>
	      <img data-bind="attr: { src: preview }">
	    </div>
	    <p>
	      Download <a data-bind="attr: { href: pdf }">PDF</a>.
	    </p>
	  <!-- /ko -->

Replace the footer button that appear when the current user is assigned to the task with:

	    <button data-bind="click: approve" class="btn btn-primary">Approve</button>
	    <button data-bind="click: dispute" class="btn btn-danger">Dispute</button>
	    <button data-bind="click: noop" class="btn">No Opinion</button>

Test the app by running `foreman start`.

**COMMIT POINT**

