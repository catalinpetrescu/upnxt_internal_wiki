# Components Spec Sheets


## DMZ

* Intake
    * [SMTP](components/Intake_SMTP.md)
    * [HTTP](components/Intake_HTTP.md)
    * FTP/S


## Frontend

* [Operations](components/Operations.md)
* [CMIS Browser](components/CMIS_Browser.md)


## Backend

* [CMIS](components/CMIS.md)
* Alexandria
    * [S3](components/Alexandria_S3.md)
    * [FileSystem](components/Alexandria_Filesystem.md)
    * [Infinispan](components/Alexandria_Infinispan.md)
    * [Postgres](components/Alexandria_Postgres.md)
* [Tasks Service](components/Task_Service.md)
* [ElasticSearch](components/ElasticSearch.md)
* [Orchestrator](components/Orchestrator.md)
* [Worker](components/Worker.md)


## Data

* Postgresql