## SSL Certificates


| Certificate | Expires | Used |
| :---------- | :-----: | :--: |
| *.nxt.unifiedpost.com | 16/12/2016 | PROD |
| *.myarchive.lu | 16/03/2017 | PROD |
| *.1850zakelijkfactuurservice.nl | 28/08/2017 | PROD |
| facturis.net, documenten.facturis.net, betalen.facturis.net | 10/04/2016 | PROD
| *.facturis.eu | 15/10/2015 | UAT |
| *.nxt.uat.unifiedpost.com | 14/12/2016 | UAT |
| *.ci.apps.up-nxt.com | 24/11/2014 | CI |
| *.all-in-one.apps.up-nxt.com | 23/10/2014 | AIO |