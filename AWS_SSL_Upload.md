## Convert the certificate to PEM

Ensure all certificates are in PEM format

	$ openssl x509 -in cmdb_upnxt.crt -outform PEM -out cmdb_upnxt.pem


## Convert the private key to RSA

Ensure the private key is in RSA format

	$ openssl rsa -in cmdb_upnxt.key -out cmdb_upnxt.rsa	

## Get the intermediate and root certificates

Find the Issuer certificate from your certificate

	$ openssl x509 -in cmdb_upnxt.pem -noout -text                           
	...
            Authority Information Access: 
                CA Issuers - URI:http://crt.comodoca.com/PositiveSSLCA2.crt
                OCSP - URI:http://ocsp.comodoca.com
	...

Download it and make sure it is in PEM format 

	$ openssl x509 -inform DER -in <(curl -s http://crt.comodoca.com/PositiveSSLCA2.crt) -outform PEM > intermediate.pem
	
Find the issuer of the intermediate certificate

	$ openssl x509 -in intermediate.pem -noout -text
	...
	            Authority Information Access: 
                CA Issuers - URI:http://crt.usertrust.com/AddTrustExternalCARoot.p7c
                CA Issuers - URI:http://crt.usertrust.com/AddTrustUTNSGCCA.crt
                OCSP - URI:http://ocsp.usertrust.com
	...
	
In this case we need to find a self-signed certificate, which is in the first PKCS7 link

	$ openssl pkcs7 -inform DER -in <(curl -s http://crt.usertrust.com/AddTrustExternalCARoot.p7c) -print_certs

Choose the self-signed CA root certificate block and save it to the file root.pem:

	subject=/C=SE/O=AddTrust AB/OU=AddTrust External TTP Network/CN=AddTrust External CA Root
	issuer=/C=SE/O=AddTrust AB/OU=AddTrust External TTP Network/CN=AddTrust External CA Root
	-----BEGIN CERTIFICATE-----
	MIIENjCCAx6gAwIBAgIBATANBgkqhkiG9w0BAQUFADBvMQswCQYDVQQGEwJTRTEU
	MBIGA1UEChMLQWRkVHJ1c3QgQUIxJjAkBgNVBAsTHUFkZFRydXN0IEV4dGVybmFs
	...
	c4g/VhsxOBi0cQ+azcgOno4uG+GMmIPLHzHxREzGBHNJdmAPx/i9F4BrLunMTA5a
	mnkPIAou1Z5jJh5VkpTYghdae9C8x49OhgQ=
	-----END CERTIFICATE-----


## Validate Certificate Chain

Validate that the certificate chain is complete


	$ openssl x509 -in root.pem -noout -issuer_hash -issuer -hash                                                                                     
	3c58f906
	issuer= /C=SE/O=AddTrust AB/OU=AddTrust External TTP Network/CN=AddTrust External CA Root
	3c58f906
	$ openssl x509 -in intermediate.pem -noout -issuer_hash -issuer -hash 
	3c58f906
	issuer= /C=SE/O=AddTrust AB/OU=AddTrust External TTP Network/CN=AddTrust External CA Root
	ed207a3e
	$ openssl x509 -in cmdb_upnxt.pem -noout -issuer_hash -issuer -hash 
	ed207a3e
	issuer= /C=GB/ST=Greater Manchester/L=Salford/O=COMODO CA Limited/CN=PositiveSSL CA 2
	c869f2b0
	
Make sure that:

* issuer_hash of cmdb_upnxt.pem == hash of intermediate.pem
* issuer_hash of intermediate.pem === hash of root.pem
* issuer_hash of root.pem == hash of root.pem

## Create the certificate chain file

Concatenate the individual certificates to 1 chain.pem file

	$ cat cmdb_upnxt.pem intermediate.pem root.pem > chain.pem

Verify the certificate is valid:

	$ openssl verify -CAfile chain.crt cmdb_upnxt.pem
	cmdb_upnxt.pem: OK

## Check AWS before upload certificate

Check for existing certificate versions

	$ aws iam list-server-certificates | grep "domain"
	"ServerCertificateName": "www.domain.com-0A.37.3C.4F.DA.A7.D1.A2.87.0C.11.FB.0F.45.CD.F0.22.81.74.47-v1", 
	
Check for amount of server certificates : Max amount of certificates is 30
	
	$ aws iam list-server-certificates | grep ServerCertificateId | wc -l
	20
	
Delete unused certificate if necessary | verify in AWS console if unused
	
	$ aws iam delete-server-certificate --server-certificate-name nxt.uat.unifiedpost.com.7D.A1.2D.EC.5A.35.F3.F0.F7.1A.89.81.EF.56.E6.6C.72.1A.CB.73-v2 

## Upload the certificate to AWS

Get the fingerprint of the certificate chain

	$ openssl x509 -noout -fingerprint -in chain.pem | cut -d= -f2 | tr : . 
	58.3C.5B.27.B9.6A.22.89.05.70.D6.53.3F.8E.00.95.D3.05.0E.81

Upload it

	$ aws iam upload-server-certificate --server-certificate-name cmdb.upnxt.com-58.3C.5B.27.B9.6A.22.89.05.70.D6.53.3F.8E.00.95.D3.05.0E.81-v1 --certificate-body file://chain.pem --private-key file://cmdb_upnxt.rsa        
